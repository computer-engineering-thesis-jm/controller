#ifndef ARDUINO_BLUETOOTH_SERVICE_H_
#define ARDUINO_BLUETOOTH_SERVICE_H_

#include <SoftwareSerial.h>
#include "ArduinoJson.h"
#include "Arduino.h"
#include "WString.h"
#include "bluetooth_service.hpp"

class ArduinoBluetoothService : public BluetoothService
{
public:
  ArduinoBluetoothService();

  virtual void connect();
  virtual void disconnect();
  virtual bool isAvailable();
  virtual Device *readDeviceConfiguration();
  virtual WifiNetwork *readWifiConfiguration();
  virtual UrlServer *readUrlServerConfiguration();
  virtual void sendCompletedAck();
  virtual void sendDeviceConfiguredAck();
  virtual void sendWifiConfiguredAck();
  virtual void sendUrlServerConfiguredAck();

  virtual ~ArduinoBluetoothService();
};

#endif