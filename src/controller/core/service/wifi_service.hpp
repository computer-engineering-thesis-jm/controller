#ifndef WIFI_SERVICE_H_
#define WIFI_SERVICE_H_

class WifiService
{
public:
  virtual void initialize() = 0;
  virtual bool initialized() = 0;
  virtual int connect(const char *ssid, const char *password) = 0;
  virtual int connectedStatus() = 0;
  virtual int disconnectedStatus() = 0;
  virtual const char* getConnectionStatus() = 0;
};

#endif