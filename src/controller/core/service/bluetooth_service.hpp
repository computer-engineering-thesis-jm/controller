#ifndef BLUETOOTH_SERVICE_H
#define BLUETOOTH_SERVICE_H

#include "../model/wifi_network.hpp"
#include "../model/url_server.hpp"
#include "../model/device.hpp"

class BluetoothService
{
public:
  virtual void connect() = 0;
  virtual void disconnect() = 0;
  virtual bool isAvailable() = 0;
  virtual Device *readDeviceConfiguration() = 0;
  virtual WifiNetwork *readWifiConfiguration() = 0;
  virtual UrlServer *readUrlServerConfiguration() = 0;
  virtual void sendCompletedAck() = 0;
  virtual void sendDeviceConfiguredAck() = 0;
  virtual void sendWifiConfiguredAck() = 0;
  virtual void sendUrlServerConfiguredAck() = 0;
};

#endif