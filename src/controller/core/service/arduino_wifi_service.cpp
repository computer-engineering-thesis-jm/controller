#include "arduino_wifi_service.hpp"

ArduinoWifiService::ArduinoWifiService()
{
}

const char *ArduinoWifiService::getConnectionStatus()
{
  const char *connectionStatus = "NONE";

  switch (WiFi.status())
  {
  case WL_NO_SHIELD:
    connectionStatus = "WL_NO_SHIELD";
    break;
  case WL_IDLE_STATUS:
    connectionStatus = "WL_IDLE_STATUS";
    break;
  case WL_CONNECTED:
    connectionStatus = "WL_CONNECTED";
    break;
  case WL_CONNECT_FAILED:
    connectionStatus = "WL_CONNECT_FAILED";
    break;
  case WL_DISCONNECTED:
    connectionStatus = "WL_DISCONNECTED";
    break;
  default:
    connectionStatus = "ERROR TO OBTAIN WIFI STATUS";
    break;
  }

  return connectionStatus;
}

void ArduinoWifiService::initialize()
{
  int resetPin = 7;
  pinMode(resetPin, OUTPUT);
  Serial.println(F("Prevent reset wifi module"));
  digitalWrite(resetPin, LOW);
  delay(2500);
  digitalWrite(resetPin, HIGH);
  delay(2500);
  Serial.println(F("Initializing..."));
  Serial1.begin(115200);
  delay(100);
  WiFi.init(&Serial1);
  delay(100);
  Serial.println(this->getConnectionStatus());
}

bool ArduinoWifiService::initialized()
{
  return WiFi.status() != WL_NO_SHIELD;
}

int ArduinoWifiService::connect(const char *ssid, const char *password)
{
  Serial.println(F("Scanning available networks..."));
  // scan for nearby networks
  int numSsid = WiFi.scanNetworks();
  if (numSsid == -1)
  {
    Serial.println(F("Couldn't get a wifi connection"));
    while (true);
  }

  // print the list of networks seen
  Serial.print(F("Number of available networks:"));
  Serial.println(numSsid);

  // print the network number and name for each network found
  for (int thisNet = 0; thisNet < numSsid; thisNet++)
  {
    Serial.print(thisNet);
    Serial.print(") ");
    Serial.print(WiFi.SSID(thisNet));
    Serial.print(F("\tSignal: "));
    Serial.print(WiFi.RSSI(thisNet));
    Serial.print(F(" dBm"));
    Serial.print(F("\tEncryption: "));
    int thisType = WiFi.encryptionType(thisNet);
    // read the encryption type and print out the name
    switch (thisType)
    {
    case ENC_TYPE_WEP:
      Serial.print("WEP");
      break;
    case ENC_TYPE_WPA_PSK:
      Serial.print("WPA_PSK");
      break;
    case ENC_TYPE_WPA2_PSK:
      Serial.print("WPA2_PSK");
      break;
    case ENC_TYPE_WPA_WPA2_PSK:
      Serial.print("WPA_WPA2_PSK");
      break;
    case ENC_TYPE_NONE:
      Serial.print("None");
      break;
    }
    Serial.println();
  }

  Serial.println(F("Connecting..."));
  int connect = WiFi.begin(ssid, password);
  Serial.println(this->getConnectionStatus());
  delay(200);
  return connect;
}

int ArduinoWifiService::connectedStatus()
{
  return WL_CONNECTED;
}

int ArduinoWifiService::disconnectedStatus()
{
  return WL_DISCONNECTED;
}

ArduinoWifiService::~ArduinoWifiService()
{
}