#ifndef ARDUINO_METRIC_SERVICE_H_
#define ARDUINO_METRIC_SERVICE_H_

#include "metric_service.hpp"
#include "WifiEspClient.h"
#include "WString.h"
#include "ArduinoJson.h"
#include "Arduino.h"

class ArduinoMetricService : public MetricService
{
public:
  ArduinoMetricService();

  virtual void sendMetric(const char *endpoint, const char *port, const char *name, const char *value, const char *deviceId);

private:
};

#endif