#include "arduino_metric_service.hpp"

ArduinoMetricService::ArduinoMetricService()
{
}

void ArduinoMetricService::sendMetric(const char *endpoint, const char *port, const char *name, const char *value, const char *deviceId)
{
  WiFiEspClient client;
  DynamicJsonBuffer jsonBuffer(512);
  JsonObject &jsonObject = jsonBuffer.createObject();
  jsonObject["deviceId"] = deviceId;
  jsonObject["name"] = name;
  jsonObject["type"] = "GAUGE";
  jsonObject["value"] = value;

  String metric;
  jsonObject.printTo(metric);

  if (client.connect(endpoint, String(port).toInt()))
  {
    delay(1000);
    Serial.print(F("Client -> Send metric: "));
    Serial.print(metric);
    Serial.println("");

    String url = String(endpoint) + ":" + port;
    client.print(String("POST /register/metric HTTP/1.1\r\n")
          + "Host: " + url + "\r\n"
					+ "Connection: close\r\n"
					+ "Content-Type: application/json\r\n" 
          + "Content-Length: " + metric.length() + "\r\n" 
          + "\r\n" + metric + "\n");
    delay(4000);

    if (client.available())
    {
      String line = client.readStringUntil('\r');
      Serial.print("Client -> Response: ");
      Serial.print(line);
      Serial.println("");
      client.flush();
    }
  }
  else
  {
    Serial.println(F("Client -> Cannot connect to server"));
  }
  client.stop();

  delay(3000);
}