#ifndef METRIC_SERVICE_H_
#define METRIC_SERVICE_H_

class MetricService
{
public:
  virtual void sendMetric(const char *endpoint, const char *port, const char *name, const char *value, const char *deviceId) = 0;
};

#endif