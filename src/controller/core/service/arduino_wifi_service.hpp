#ifndef ARDUINO_WIFI_SERVICE_H_
#define ARDUINO_WIFI_SERVICE_H_

#include "wifi_service.hpp"
#include "WiFiEsp.h"
#include "WiFiEspClient.h"

class ArduinoWifiService : public WifiService
{
public:
  ArduinoWifiService();

  virtual void initialize();
  virtual bool initialized();
  virtual int connect(const char *ssid, const char *password);
  virtual int connectedStatus();
  virtual int disconnectedStatus();
  virtual const char* getConnectionStatus();

  virtual ~ArduinoWifiService();
};

#endif