#include "arduino_bluetooth_service.hpp"

ArduinoBluetoothService::ArduinoBluetoothService()
{
}

void ArduinoBluetoothService::connect()
{
   Serial.println(F("btc"));
   pinMode(8, OUTPUT);
   pinMode(9, OUTPUT);
   digitalWrite(9, HIGH);
   delay(500);
   digitalWrite(8, HIGH);
   Serial2.begin(9600);
   delay(500);
}

void ArduinoBluetoothService::disconnect()
{
   Serial.println(F("btd"));
   digitalWrite(9, LOW);
   digitalWrite(8, LOW);
}

bool ArduinoBluetoothService::isAvailable()
{
   int availableBytes = Serial2.available();
   delay(300);
   return availableBytes > 1;
}

Device *ArduinoBluetoothService::readDeviceConfiguration()
{
   String result = String("");
   int bytesToRead = Serial2.available();
   Serial.println(F("cd"));
   Serial.print("dAB=");
   Serial.println(bytesToRead);
   for (int i = 0; i < bytesToRead; i++)
   {
      char readed = (char)Serial2.read();
      result.concat(readed);
   }

   Serial.print(F("R= "));
   Serial.println(result);
   Serial.println(F("rd"));

   DynamicJsonBuffer jsonBuffer(512);
   JsonObject &jsonObject = jsonBuffer.parseObject(result.c_str());
   const char *deviceId = jsonObject.get<const char *>("did");
   const char *token = jsonObject.get<const char *>("tkn");
   return new Device(deviceId, token);
}

WifiNetwork *ArduinoBluetoothService::readWifiConfiguration()
{
   String result = String("");
   int bytesToRead = Serial2.available();
   Serial.println(F("cw"));
   Serial.print("wAB=");
   Serial.println(bytesToRead);
   for (int i = 0; i < bytesToRead; i++)
   {
      char readed = (char)Serial2.read();
      result.concat(readed);
   }

   Serial.print(F("R= "));
   Serial.println(result);
   Serial.println(F("rw"));

   DynamicJsonBuffer jsonBuffer(512);
   JsonObject &jsonObject = jsonBuffer.parseObject(result.c_str());
   const char *ssid = jsonObject.get<const char *>("sid");
   const char *password = jsonObject.get<const char *>("pss");

   return new WifiNetwork(ssid, password);
}

UrlServer *ArduinoBluetoothService::readUrlServerConfiguration()
{
   String result = String("");
   int bytesToRead = Serial2.available();
   Serial.println(F("cu"));
   Serial.print("uAB=");
   Serial.println(bytesToRead);
   for (int i = 0; i < bytesToRead; i++)
   {
      char readed = (char)Serial2.read();
      result.concat(readed);
   }

   Serial.print(F("R= "));
   Serial.println(result);
   Serial.println(F("ru"));

   DynamicJsonBuffer jsonBuffer(512);
   JsonObject &jsonObject = jsonBuffer.parseObject(result.c_str());
   const char *endpoint = jsonObject.get<const char *>("url");
   const char *port = jsonObject.get<const char *>("prt");
   return new UrlServer(endpoint, port);
}

void ArduinoBluetoothService::sendCompletedAck()
{
   Serial2.write('c');
   delay(200);
   Serial.println(F("c"));
}

void ArduinoBluetoothService::sendDeviceConfiguredAck()
{
   Serial2.write('d');
   delay(200);
   Serial.println(F("d"));
}

void ArduinoBluetoothService::sendWifiConfiguredAck()
{
   Serial2.write('w');
   delay(200);
   Serial.println(F("w"));
}

void ArduinoBluetoothService::sendUrlServerConfiguredAck()
{
   Serial2.write('u');
   delay(200);
   Serial.println(F("u"));
}

ArduinoBluetoothService::~ArduinoBluetoothService()
{
}