#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include "../model/wifi_network.hpp"
#include "../model/url_server.hpp"
#include "../model/device.hpp"

class Configuration
{

public:
	Configuration();

	bool isDeviceConfigured();
	bool isWifiConfigured();
	bool isUrlServerConfigured();

	void putWifiNetwork(WifiNetwork *wifiNetwork);
	WifiNetwork *getWifiNetwork();

	void putUrlServer(UrlServer *urlServer);
	UrlServer *getUrlServer();

	void putDevice(Device *device);
	Device *getDevice();

	virtual ~Configuration();

private:
	WifiNetwork *wifiNetwork;
	UrlServer *urlServer;
	Device *device;
};

#endif