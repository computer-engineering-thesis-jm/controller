#ifndef METRICS_H_
#define METRICS_H_

#include "../model/metric/basic_metric.hpp"
#include "../model/metric/electricity_switch_metric.hpp"
#include "../model/metric/light_metric.hpp"

class Metrics
{
  public:
	Metrics();

	void putBasicMetric(BasicMetric *basicMetric);
	BasicMetric* getBasicMetric();

	void putLightMetric(LightMetric *lightMetric);
	LightMetric* getLightMetric();
	
	void putElectricitySwitchMetric(ElectricitySwitchMetric *electricitySwitchMetric);
	ElectricitySwitchMetric* getElectricitySwitchMetric();

	virtual ~Metrics();

  private:
	BasicMetric *basicMetric;
	LightMetric *lightMetric;
	ElectricitySwitchMetric *electricitySwitchMetric;
};

#endif