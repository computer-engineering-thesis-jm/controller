#include "metrics.hpp"

Metrics::Metrics()
{
	this->basicMetric = new BasicMetric("metric_1", "1");
	this->lightMetric = new LightMetric("light", "0");
	this->electricitySwitchMetric = new ElectricitySwitchMetric("electricity_switch", "0");
}

void Metrics::putBasicMetric(BasicMetric *basicMetric)
{
	this->basicMetric = basicMetric;
}

BasicMetric* Metrics::getBasicMetric()
{
	return basicMetric;
}

void Metrics::putLightMetric(LightMetric *lightMetric)
{
	this->lightMetric = lightMetric;
}

LightMetric* Metrics::getLightMetric()
{
	return lightMetric;
}

void Metrics::putElectricitySwitchMetric(ElectricitySwitchMetric *electricitySwitchMetric)
{
	this->electricitySwitchMetric = electricitySwitchMetric;
}

ElectricitySwitchMetric* Metrics::getElectricitySwitchMetric()
{
	return electricitySwitchMetric;
}

Metrics::~Metrics()
{
}