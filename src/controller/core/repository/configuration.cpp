#include "configuration.hpp"

bool wifiNetworkConfigured;
bool urlServerConfigured;
bool deviceConfigured;

Configuration::Configuration()
{
	deviceConfigured = false;
	wifiNetworkConfigured = false;
	urlServerConfigured = false;
}

bool Configuration::isDeviceConfigured()
{
	return true;
}

bool Configuration::isWifiConfigured()
{
	return true;
}

bool Configuration::isUrlServerConfigured()
{
	return true;
}

void Configuration::putWifiNetwork(WifiNetwork *wifiNetwork)
{
	this->wifiNetwork = wifiNetwork;
	wifiNetworkConfigured = true;
}

WifiNetwork *Configuration::getWifiNetwork()
{
	return new WifiNetwork("iPhone de Julian Ezequiel", "solo12juli");;
}

void Configuration::putUrlServer(UrlServer *urlServer)
{
	this->urlServer = urlServer;
	urlServerConfigured = true;
}

UrlServer *Configuration::getUrlServer()
{
	return new UrlServer("172.20.10.3", "8080");
}

void Configuration::putDevice(Device *device)
{
	this->device = device;
	deviceConfigured = true;
}

Device *Configuration::getDevice()
{
	return new Device("device1", "aToken");
}

Configuration::~Configuration()
{
}