#include "should_switch_electricity_source.hpp"

ShouldSwitchElectricitySource::ShouldSwitchElectricitySource(Sensor *currentSensor, ElectricitySwitch *electricitySwitch, Metrics *metrics)
{
    this->currentSensor = currentSensor;
    this->electricitySwitch = electricitySwitch;
    this->metrics = metrics;
}

void ShouldSwitchElectricitySource::execute()
{
    float sensorValue = this->currentSensor->get();
    const char *value = "0";
    if(sensorValue > -0.12) {
        this->electricitySwitch->turnOff();
        value = "0";
    } else {
        this->electricitySwitch->turnOn();
        value = "1";
    }
    
    ElectricitySwitchMetric *electricitySwitchMetric = new ElectricitySwitchMetric("electricity_switch", value);
    this->metrics->putElectricitySwitchMetric(electricitySwitchMetric);
}

ShouldSwitchElectricitySource::~ShouldSwitchElectricitySource()
{
}