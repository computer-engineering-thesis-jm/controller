#ifndef REGISTER_METRICS_H_
#define REGISTER_METRICS_H_

#include "../repository/configuration.hpp"
#include "../repository/metrics.hpp"
#include "../service/metric_service.hpp"
#include "../model/metric/basic_metric.hpp"
#include "../model/metric/electricity_switch_metric.hpp"
#include "../model/metric/light_metric.hpp"

// Delete after testing
#include "Arduino.h"

class RegisterMetrics
{
  public:
	RegisterMetrics(Configuration *configuration, Metrics *metrics, MetricService *metricService);

	void execute();

	virtual ~RegisterMetrics();

  private:
	Configuration *configuration;
	Metrics *metrics;
	MetricService *metricService;
};

#endif
