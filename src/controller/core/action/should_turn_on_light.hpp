#ifndef SHOULD_TURN_ON_LIGHT_H_
#define SHOULD_TURN_ON_LIGHT_H_

#include "../model/metric/light_metric.hpp"
#include "../model/metric/electricity_switch_metric.hpp"
#include "../component_header.hpp"
#include "../repository/metrics.hpp"

class ShouldTurnOnLight
{
public:
  ShouldTurnOnLight(Sensor *lightSensor, Light *light, ElectricitySwitch *electricitySwitch, Metrics *metrics);
  void execute();

  virtual ~ShouldTurnOnLight();

private:
  Sensor *lightSensor;
  Light *light;
  ElectricitySwitch *electricitySwitch;
  Metrics *metrics;
};

#endif