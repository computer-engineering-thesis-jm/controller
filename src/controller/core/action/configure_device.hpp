#ifndef CONFIGURE_DEVICE_H_
#define CONFIGURE_DEVICE_H_

#include "../service/bluetooth_service.hpp"
#include "../repository/configuration.hpp"

class ConfigureDevice
{
public:
  ConfigureDevice(BluetoothService *bluetoothService, Configuration *configuration);

  void execute();

  virtual ~ConfigureDevice();

private:
  BluetoothService *bluetoothService;
  Configuration *configuration;
};

#endif