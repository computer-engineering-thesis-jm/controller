#include "register_metrics.hpp"

RegisterMetrics::RegisterMetrics(Configuration *configuration, Metrics *metrics, MetricService *metricService)
{
  this->configuration = configuration;
  this->metrics = metrics;
  this->metricService = metricService;
}

void RegisterMetrics::execute()
{
  Device *device = this->configuration->getDevice();
  UrlServer *urlServer = this->configuration->getUrlServer();
  BasicMetric *basicMetric = this->metrics->getBasicMetric();
  LightMetric *lightMetric = this->metrics->getLightMetric();
  ElectricitySwitchMetric *electricitySwitchMetric = this->metrics->getElectricitySwitchMetric();

  this->metricService->sendMetric(urlServer->getEndpoint(), urlServer->getPort(), basicMetric->getName(), basicMetric->getValue(), device->getId());
  this->metricService->sendMetric(urlServer->getEndpoint(), urlServer->getPort(), lightMetric->getName(), lightMetric->getValue(), device->getId());
  this->metricService->sendMetric(urlServer->getEndpoint(), urlServer->getPort(), electricitySwitchMetric->getName(), electricitySwitchMetric->getValue(), device->getId());
}

RegisterMetrics::~RegisterMetrics()
{
} 