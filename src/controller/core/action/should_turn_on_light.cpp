#include "should_turn_on_light.hpp"

ShouldTurnOnLight::ShouldTurnOnLight(Sensor *lightSensor, Light *light, ElectricitySwitch *electricitySwitch, Metrics *metrics)
{
    this->lightSensor = lightSensor;
    this->light = light;
    this->electricitySwitch = electricitySwitch;
    this->metrics = metrics;
}

void ShouldTurnOnLight::execute()
{
    const char* value = "0";
    int sensorValue = this->lightSensor->get();
    if(sensorValue >= 0 && sensorValue <= 10) {
        value = "1";
        this->light->turnOn();
    } else {
        value = "0";
        this->light->turnOff();
        this->electricitySwitch->turnOn();
    }
    LightMetric *lightMetric = new LightMetric("light", value);
    this->metrics->putLightMetric(lightMetric);
    ElectricitySwitchMetric *electricitySwitchMetric = new ElectricitySwitchMetric("electricity_switch", value);
    this->metrics->putElectricitySwitchMetric(electricitySwitchMetric);
}

ShouldTurnOnLight::~ShouldTurnOnLight()
{
}