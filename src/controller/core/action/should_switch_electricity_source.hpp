#ifndef SHOULD_SWITCH_ELECTRICITY_SOURCE_H_
#define SHOULD_SWITCH_ELECTRICITY_SOURCE_H_

#include "../model/metric/electricity_switch_metric.hpp"
#include "../component_header.hpp"
#include "../repository/metrics.hpp"

class ShouldSwitchElectricitySource
{
public:
  ShouldSwitchElectricitySource(Sensor *currentSensor, ElectricitySwitch *electricitySwitch, Metrics *metrics);
  void execute();

  virtual ~ShouldSwitchElectricitySource();

private:
  Sensor *currentSensor;
  ElectricitySwitch *electricitySwitch;
  Metrics *metrics;
};

#endif