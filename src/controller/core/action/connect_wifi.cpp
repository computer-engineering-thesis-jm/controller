#include "connect_wifi.hpp"

ConnectWifi::ConnectWifi(WifiService *wifiService, Configuration *configuration)
{
	this->wifiService = wifiService;
	this->configuration = configuration;
}

bool ConnectWifi::execute()
{
	do {
		this->wifiService->initialize();
	} while(!this->wifiService->initialized());

	int status = 0;
	int retry = 0;
	status = this->wifiService->disconnectedStatus();
	WifiNetwork* wifiNetwork = this->configuration->getWifiNetwork();

	while (status != this->wifiService->connectedStatus() && retry < 3)
	{
		status = this->wifiService->connect(wifiNetwork->getSsid(), wifiNetwork->getPassword());
		retry++;
	}

	bool connected = this->wifiService->connectedStatus() != 0;
	return connected;
}

ConnectWifi::~ConnectWifi()
{
}