#include "configure_device.hpp"

ConfigureDevice::ConfigureDevice(BluetoothService *bluetoothService, Configuration *configuration)
{
	this->bluetoothService = bluetoothService;
	this->configuration = configuration;
}

void ConfigureDevice::execute()
{
	// Connect Bluetooth
	this->bluetoothService->connect();

	// Configure Device
	while (!this->configuration->isDeviceConfigured())
	{
		if (this->bluetoothService->isAvailable())
		{
			Device *device = this->bluetoothService->readDeviceConfiguration();
			this->configuration->putDevice(device);
			this->bluetoothService->sendDeviceConfiguredAck();
		}
	}

	// Configure Wifi
	while (!this->configuration->isWifiConfigured())
	{
		if (this->bluetoothService->isAvailable())
		{
			WifiNetwork *wifiNetwork = this->bluetoothService->readWifiConfiguration();
			this->configuration->putWifiNetwork(wifiNetwork);
			this->bluetoothService->sendWifiConfiguredAck();
		}
	}

	// Configure Url Server
	while (!this->configuration->isUrlServerConfigured())
	{
		if (this->bluetoothService->isAvailable())
		{
			UrlServer *urlServer = this->bluetoothService->readUrlServerConfiguration();
			this->configuration->putUrlServer(urlServer);
			this->bluetoothService->sendUrlServerConfiguredAck();
		}
	}

	// TODO: read time from json object

	// Send received confirmation
	this->bluetoothService->sendCompletedAck();

	// Disconnect Bluetooth
	this->bluetoothService->disconnect();
}

ConfigureDevice::~ConfigureDevice()
{
}
