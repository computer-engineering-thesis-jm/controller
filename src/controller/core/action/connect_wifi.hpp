#ifndef CONNECT_WIFI_H_
#define CONNECT_WIFI_H_

#include "../service/wifi_service.hpp"
#include "../repository/configuration.hpp"

class ConnectWifi
{

public:
	ConnectWifi(WifiService *wifiService, Configuration *configuration);

	bool execute();

	virtual ~ConnectWifi();

private:
	WifiService *wifiService;
	Configuration *configuration;
};

#endif