#ifndef DEVICE_H_
#define DEVICE_H_

class Device
{

public:
	Device(const char *id, const char *token);

	const char *getId();
	const char *getToken();
	bool operator==(const Device& rhs) const{
    	return this->id == rhs.id && this->token == rhs.token;
	}

	virtual ~Device();

private:
	const char *id;
	const char *token;
};

#endif