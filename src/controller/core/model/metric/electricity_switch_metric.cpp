#include "electricity_switch_metric.hpp"

ElectricitySwitchMetric::ElectricitySwitchMetric(const char *name, const char *value)
{
	this->name = name;
	this->value = value;
}

const char *ElectricitySwitchMetric::getName()
{
	return name;
}

const char *ElectricitySwitchMetric::getValue()
{
	return value;
}

ElectricitySwitchMetric::~ElectricitySwitchMetric()
{
}