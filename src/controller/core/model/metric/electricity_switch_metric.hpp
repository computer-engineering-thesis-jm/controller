#ifndef ELECTRICITY_SWITCH_METRIC_H_
#define ELECTRICITY_SWITCH_METRIC_H_

class ElectricitySwitchMetric
{

  public:
	ElectricitySwitchMetric(const char *name, const char *value);

	const char *getName();
	const char *getValue();

	virtual ~ElectricitySwitchMetric();

  private:
	const char *name;
	const char *value;
};

#endif