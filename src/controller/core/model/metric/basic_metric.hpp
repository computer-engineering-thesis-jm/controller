#ifndef BASIC_METRIC_H_
#define BASIC_METRIC_H_

class BasicMetric
{

  public:
	BasicMetric(const char *name, const char *value);

	const char *getName();
	const char *getValue();

	virtual ~BasicMetric();

  private:
	const char *name;
	const char *value;
};

#endif