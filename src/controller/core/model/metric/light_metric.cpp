#include "light_metric.hpp"

LightMetric::LightMetric(const char *name, const char *value)
{
	this->name = name;
	this->value = value;
}

const char *LightMetric::getName()
{
	return name;
}

const char *LightMetric::getValue()
{
	return value;
}

LightMetric::~LightMetric()
{
}