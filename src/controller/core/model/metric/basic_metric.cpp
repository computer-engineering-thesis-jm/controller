#include "basic_metric.hpp"

BasicMetric::BasicMetric(const char *name, const char *value)
{
	this->name = name;
	this->value = value;
}

const char *BasicMetric::getName()
{
	return name;
}

const char *BasicMetric::getValue()
{
	return value;
}

BasicMetric::~BasicMetric()
{
}