#ifndef LIGHT_METRIC_H_
#define LIGHT_METRIC_H_

class LightMetric
{

  public:
	LightMetric(const char *name, const char *value);

	const char *getName();
	const char *getValue();

	virtual ~LightMetric();

  private:
	const char *name;
	const char *value;
};

#endif