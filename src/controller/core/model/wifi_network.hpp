#ifndef WIFI_NETWORK_H_
#define WIFI_NETWORK_H_

class WifiNetwork
{

public:
	WifiNetwork(const char *ssid, const char *password);

	const char *getSsid();
	const char *getPassword();
	bool operator==(const WifiNetwork& rhs) const{
    	return this->ssid == rhs.ssid && this->password == rhs.password;
	}

	virtual ~WifiNetwork();

private:
	const char *ssid;
	const char *password;
};

#endif
