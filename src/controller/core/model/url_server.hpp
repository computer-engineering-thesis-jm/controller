#ifndef URL_SERVER_H_
#define URL_SERVER_H_

class UrlServer
{

public:
	UrlServer(const char *endpoint, const char *port);

	const char *getEndpoint();
	const char *getPort();

	virtual ~UrlServer();

private:
	const char *endpoint;
	const char *port;
};

#endif
