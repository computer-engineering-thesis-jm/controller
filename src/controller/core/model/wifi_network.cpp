#include "wifi_network.hpp"

WifiNetwork::WifiNetwork(const char *ssid, const char *password)
{
	this->ssid = ssid;
	this->password = password;
}

const char *WifiNetwork::getSsid()
{
	return ssid;
}

const char *WifiNetwork::getPassword()
{
	return password;
}

WifiNetwork::~WifiNetwork()
{
}
