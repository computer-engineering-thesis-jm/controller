#include "device.hpp"

Device::Device(const char *id, const char *token)
{
	this->id = id;
	this->token = token;
}

const char *Device::getId()
{
	return id;
}

const char *Device::getToken()
{
	return token;
}

Device::~Device()
{
}
