#include "url_server.hpp"

UrlServer::UrlServer(const char *endpoint, const char *port)
{
	this->endpoint = endpoint;
	this->port = port;
}

const char *UrlServer::getEndpoint()
{
	return endpoint;
}

const char *UrlServer::getPort()
{
	return port;
}

UrlServer::~UrlServer()
{
}
