#ifndef CURRENT_SENSOR_H_
#define CURRENT_SENSOR_H_

#include "sensor.hpp"
#include "Arduino.h"

class CurrentSensor : public Sensor
{
public:
  CurrentSensor();

  virtual float get();

  virtual ~CurrentSensor();
  
private:
  uint8_t pin;
};

#endif