#include "current_sensor.hpp"

float sensitivity[] = {
  0.185,// for ACS712ELCTR-05B-T
  0.100,// for ACS712ELCTR-20A-T
  0.066 // for ACS712ELCTR-30A-T
};

const float VCC = 5.0; // supply voltage is from 4.5 to 5.5V. Normally 5V.
const int model = 2; // enter the model number (see below)
float cutOffLimit = 1.01; // set the current which below that value, doesn't matter. Or set 0.5
const float QOV = 0.5 * VCC; // set quiescent Output voltage of 0.5V
float voltage; // internal variable for voltage

CurrentSensor::CurrentSensor()
{
  this->pin = A1;
}

float CurrentSensor::get()
{
    float voltage_raw = (5.0 / 1023.0) * analogRead(this->pin); // Read the voltage from sensor
    voltage = voltage_raw - QOV + 0.012 ; // 0.000 is a value to make voltage zero when there is no current
    float current = voltage / sensitivity[model];
    Serial.print("CurrentSensor: value=");
    Serial.print(current);
    Serial.println("");
    return current;
}

CurrentSensor::~CurrentSensor()
{
}