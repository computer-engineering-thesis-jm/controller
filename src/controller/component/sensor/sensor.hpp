#ifndef SENSOR_H_
#define SENSOR_H_

class Sensor
{
public:
  virtual float get() = 0;
};

#endif