#ifndef LUX_METER_SENSOR_H_
#define LUX_METER_SENSOR_H_

#include "Arduino.h"
#include "sensor.hpp"

/* Lux Meter Sensor
   
   Connections
   ===========
   Connect the photoresistor between 5V and A0 analog input pin of Arduino. 
   Connect the 10K resistor between A0 and GROUND pin of Arduino. 

*/

class LuxMeterSensor : public Sensor
{
public:
  LuxMeterSensor();

  virtual float get();

  virtual ~LuxMeterSensor();

private:
  uint8_t pin;
};

#endif