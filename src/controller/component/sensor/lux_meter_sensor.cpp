#include "lux_meter_sensor.hpp"

LuxMeterSensor::LuxMeterSensor()
{
  this->pin = A0;
}

float LuxMeterSensor::get()
{
  int sensorValue = 0;
  unsigned int curOutputValue = 0;
  sensorValue = analogRead(this->pin);
  // map input values between 0 t0 100%
  curOutputValue = map(sensorValue, 0, 690, 0, 100);

  if (curOutputValue > 100)
  {
    curOutputValue = 100;
  }

  Serial.print("LuxMeterSensor: value=");
  Serial.print(curOutputValue);
  Serial.println("");
  return curOutputValue;
}

LuxMeterSensor::~LuxMeterSensor()
{
}