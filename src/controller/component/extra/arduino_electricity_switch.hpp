#ifndef ARDUINO_ELECTRICITY_SWITCH_H_
#define ARDUINO_ELECTRICITY_SWITCH_H_

#include "electricity_switch.hpp"
#include "Arduino.h"

class ArduinoElectricitySwitch : public ElectricitySwitch
{
public:
  ArduinoElectricitySwitch();
  
  virtual void turnOn();
  virtual void turnOff();
};

#endif