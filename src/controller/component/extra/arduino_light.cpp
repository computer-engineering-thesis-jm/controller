#include "arduino_light.hpp"

ArduinoLight::ArduinoLight()
{
    //pinMode(10, OUTPUT);
    //pinMode(11, OUTPUT);
}

void ArduinoLight::turnOn()
{
    Serial.println("Light Component-> ON");
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
}

void ArduinoLight::turnOff()
{
    Serial.println("Light Component-> OFF");
    digitalWrite(10, HIGH);
    digitalWrite(11, HIGH);
}