#ifndef LIGHT_H_
#define LIGHT_H_

class Light
{
public:
  virtual void turnOn() = 0;
  virtual void turnOff() = 0;
};

#endif