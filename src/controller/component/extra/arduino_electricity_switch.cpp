#include "arduino_electricity_switch.hpp"

ArduinoElectricitySwitch::ArduinoElectricitySwitch()
{
    //pinMode(12, OUTPUT);
    //pinMode(13, OUTPUT);
}

void ArduinoElectricitySwitch::turnOn()
{
    Serial.println("SwitchElectricity: Solar module");
    digitalWrite(12, LOW); 
    digitalWrite(13, LOW);
}

void ArduinoElectricitySwitch::turnOff()
{
    Serial.println("SwitchElectricity: Network");
    digitalWrite(12, HIGH);
    digitalWrite(13, HIGH);
}