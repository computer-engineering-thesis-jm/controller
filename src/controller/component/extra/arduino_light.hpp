#ifndef ARDUINO_LIGHT_H_
#define ARDUINO_LIGHT_H_

#include "Arduino.h"
#include "light.hpp"

class ArduinoLight : public Light
{
/* Arduino Light

    Connection
    ==========
    Connect to pin 1
*/
public:
  ArduinoLight();

  virtual void turnOn();
  virtual void turnOff();
private:
  int pin;
};

#endif