#ifndef ELECTRICITY_SWITCH_H_
#define ELECTRICITY_SWITCH_H_

class ElectricitySwitch
{
public:
  virtual void turnOn() = 0;
  virtual void turnOff() = 0;
};

#endif