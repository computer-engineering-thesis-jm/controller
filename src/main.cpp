#include "Arduino.h"
#include "WifiEspClient.h"

#include "controller/core/action/configure_device.hpp"
#include "controller/core/action/connect_wifi.hpp"
#include "controller/core/action/register_metrics.hpp"
#include "controller/core/action/should_turn_on_light.hpp"
#include "controller/core/action/should_switch_electricity_source.hpp"

#include "controller/core/service/arduino_bluetooth_service.hpp"
#include "controller/core/service/arduino_wifi_service.hpp"
#include "controller/core/service/arduino_metric_service.hpp"

#include "controller/component/extra/arduino_light.hpp"
#include "controller/component/extra/arduino_electricity_switch.hpp"

#include "controller/component/sensor/lux_meter_sensor.hpp"
#include "controller/component/sensor/current_sensor.hpp"

ShouldSwitchElectricitySource *shouldSwitchElectricitySource;
ShouldTurnOnLight *shouldTurnOnLight;
RegisterMetrics *registerMetrics;

void setup()
{
	Serial.begin(9600);
	Serial.println(F("Setup"));

	pinMode(10, OUTPUT);
	digitalWrite(10, HIGH);
    pinMode(11, OUTPUT);
	digitalWrite(11, HIGH);
	pinMode(12, OUTPUT);
	digitalWrite(12, HIGH);
    pinMode(13, OUTPUT);
	digitalWrite(13, HIGH);

	Configuration *configuration = new Configuration();
	Metrics *metrics = new Metrics();

	ArduinoBluetoothService *arduinoBluetoothService = new ArduinoBluetoothService();
	ConfigureDevice *configureDevice = new ConfigureDevice(arduinoBluetoothService, configuration);
	configureDevice->execute();
	delete configureDevice;
	delete arduinoBluetoothService;

	ArduinoWifiService *arduinoWifiService = new ArduinoWifiService();
	ConnectWifi *connectWifi = new ConnectWifi(arduinoWifiService, configuration);
	connectWifi->execute();
	delete connectWifi;
	delete arduinoWifiService;

	ArduinoElectricitySwitch *arduinoElectricitySwitch = new ArduinoElectricitySwitch();
	CurrentSensor *currentSensor = new CurrentSensor();
	shouldSwitchElectricitySource = new ShouldSwitchElectricitySource(currentSensor, arduinoElectricitySwitch, metrics);

	ArduinoLight *arduinoLight = new ArduinoLight();
	LuxMeterSensor *luxMeterSensor = new LuxMeterSensor();
	shouldTurnOnLight = new ShouldTurnOnLight(luxMeterSensor, arduinoLight, arduinoElectricitySwitch, metrics);
	
	ArduinoMetricService *arduinoMetricService = new ArduinoMetricService();
	registerMetrics = new RegisterMetrics(configuration, metrics, arduinoMetricService);
}

void loop()
{
	shouldSwitchElectricitySource->execute();
	shouldTurnOnLight->execute();
	registerMetrics->execute();
	delay(5000);
}
