add_library(controller "")

target_sources(
    controller
    PRIVATE
        controller/core/action/configure_device.cpp
        controller/core/action/register_metrics.cpp
        controller/core/action/connect_wifi.cpp
        controller/core/action/should_turn_on_light.cpp
        controller/core/action/should_switch_electricity_source.cpp
        controller/core/model/device.cpp
        controller/core/model/url_server.cpp
        controller/core/model/wifi_network.cpp
        controller/core/repository/configuration.cpp
        controller/core/repository/metrics.cpp
        controller/core/model/metric/basic_metric.cpp
        controller/core/model/metric/light_metric.cpp
        controller/core/model/metric/electricity_switch_metric.cpp
          
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/action/configure_device.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/action/register_metrics.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/action/connect_wifi.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/action/should_turn_on_light.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/action/should_switch_electricity_source.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/model/device.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/model/url_server.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/model/wifi_network.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/model/metric/basic_metric.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/model/metric/light_metric.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/model/metric/electricity_switch_metric.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/repository/configuration.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/repository/metrics.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/service/bluetooth_service.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/service/wifi_service.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/core/service/metric_service.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/component/extra/light.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/component/extra/electricity_switch.hpp
        ${CMAKE_CURRENT_LIST_DIR}/controller/component/sensor/sensor.hpp
    )

target_include_directories(
    controller
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}
    )