#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>

#include "../source_header.h"
#include "mock_wifi_service.cpp"

using ::testing::AtLeast;
using ::testing::Return;

TEST(ConnectWifiTest, connectWifiThroughWifiService)
{
    // Given
    MockWifiService *mockWifiService = new MockWifiService();
    Configuration *configuration = new Configuration();
    
    ConnectWifi *connectWifi = new ConnectWifi(mockWifiService, configuration);

    const char* deviceId = "aDeviceId";
    const char* tokenId = "aTokenId";
    const char* ssid = "anSSID";
    const char* password = "aPassword";
    const char* endpoint = "anEndpoint";
    const char* port = "aPort";

    Device *device = new Device(deviceId, tokenId);
    WifiNetwork *wifiNetwork = new WifiNetwork(ssid, password);
    UrlServer *urlServer = new UrlServer(endpoint, port);
    configuration->putDevice(device);
    configuration->putWifiNetwork(wifiNetwork);
    configuration->putUrlServer(urlServer);

    int connectedStatus = 1;
    int disconnectedStatus = 0;
    
    EXPECT_CALL(*mockWifiService, initialize())
        .Times(1);

    EXPECT_CALL(*mockWifiService, initialized())
        .Times(1)
        .WillOnce(Return(true));

    EXPECT_CALL(*mockWifiService, connect(ssid, password))
        .Times(1)
        .WillOnce(Return(connectedStatus));

    EXPECT_CALL(*mockWifiService, connectedStatus())
        .Times(4)
        .WillRepeatedly(Return(connectedStatus));

    EXPECT_CALL(*mockWifiService, disconnectedStatus())
        .Times(1)
        .WillOnce(Return(disconnectedStatus));
    
    // When
    bool actualResult = connectWifi->execute();

    // Then
    ASSERT_TRUE(actualResult);

    delete mockWifiService;
    delete connectWifi;
    delete configuration;
    delete device;
    delete wifiNetwork;
    delete urlServer;
}