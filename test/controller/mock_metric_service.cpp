#include "gmock/gmock.h"
#include "../source_header.h"

class MockMetricService : public MetricService
{
public:
  MOCK_METHOD3(send, bool(const char *, const char *, const char *));
};