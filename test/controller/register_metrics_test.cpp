#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>

#include "../source_header.h"

#include "mock_http_client.cpp"
#include "mock_json_parser.cpp"
#include "mock_json_object.cpp"
#include "mock_string_builder.cpp"

using ::testing::AtLeast;
using ::testing::Return;

TEST(RegisterMetricsTest, registerMetricsFromRepositoryThroughHttpClient)
{
    // Given
    Configuration *configuration = new Configuration();
    Metrics *metrics = new Metrics();
    MockMetricService *mockMetricService = new MockMetricService();

    RegisterMetrics *registerMetrics = new RegisterMetrics(configuration, metrics, mockMetricService);

    const char* deviceId = "aDeviceId";
    const char* tokenId = "aTokenId";
    const char* ssid = "anSSID";
    const char* password = "aPassword";
    const char* endpoint = "anEndpoint";
    const char* port = "aPort";

    Device *device = new Device(deviceId, tokenId);
    WifiNetwork *wifiNetwork = new WifiNetwork(ssid, password);
    UrlServer *urlServer = new UrlServer(endpoint, port);
    configuration->putDevice(device);
    configuration->putWifiNetwork(wifiNetwork);
    configuration->putUrlServer(urlServer);

    const char* name = "aMetricName";
    const char* value = "anyValue";
    BasicMetric *basicMetric = new BasicMetric(name, value);
    LightMetric *lightMetric = new LightMetric(name, value);
    ElectricitySourceMetric *electricitySourceMetric = new ElectricitySourceMetric(name, value);
    metrics->putBasicMetric(basicMetric);
    metrics->putLightMetric(lightMetric);
    metrics->putElectricitySourceMetric(electricitySourceMetric);

    //mock basic metric: ->createMetric()
    //mock light metric: ->createMetric()
    //mock electricity source metric: ->createMetric()

    std::string jsonObjectAsString = std::string("{\"device_id\":") + std::string(device->getId()) + std::string(",\r\n") +
            std::string("\"name\":") + std::string(basicMetric->getName()) + std::string(",\r\n") +
            std::string("\"type\":") + std::string("GAUGE") + std::string(",\r\n") +
            std::string("\"value\":") + std::string(basicMetric->getValue()) + std::string("\r\n") +
            std::string("}");
    
    const char *content = jsonObjectAsString.c_str();
    int contentLength = strlen(content);

    std::string metricBuilded = std::string("POST /register/metric Host: ") + 
            std::string(urlServer->getEndpoint()) + std::string(":") + std::string(urlServer->getPort()) + std::string("\r\n") +
            std::string("Content-Length: ") + std::to_string(contentLength) + std::string("\r\n") +
            std::string("Content-Type: application/json;charset=UTF-8\r\n\r\n") + std::string(content) + std::string("\n");
    
    EXPECT_CALL(*mockMetricService, send(endpoint, port, metricBuilded.c_str()))
        .Times(3)
        .WillRepeatedly(Return(true));

    // When
    bool actualResult = registerMetrics->execute();

    // Then
    ASSERT_TRUE(actualResult);

    delete registerMetrics; 
    delete configuration;
    delete metrics;
    delete mockMetricService;
    delete device;
    delete wifiNetwork;
    delete urlServer;
    delete basicMetric;
}