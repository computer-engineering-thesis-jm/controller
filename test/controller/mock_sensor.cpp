#include "gmock/gmock.h"
#include "../source_header.h"

class MockSensor : public Sensor
{
public:
  MOCK_METHOD0(get, int());
};