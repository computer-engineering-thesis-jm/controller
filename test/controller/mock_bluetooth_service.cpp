#include "gmock/gmock.h"
#include "../source_header.h"

class MockBluetoothService : public BluetoothService
{
public:
  MOCK_METHOD0(connect, void());
  MOCK_METHOD0(disconnect, void());
  MOCK_METHOD0(isAvailable, bool());
  MOCK_METHOD0(readDeviceConfiguration, Device*());
  MOCK_METHOD0(readWifiConfiguration, WifiNetwork*());
  MOCK_METHOD0(readUrlServerConfiguration, UrlServer*());
  MOCK_METHOD0(sendCompletedAck, void());
  MOCK_METHOD0(sendDeviceConfiguredAck, void());
  MOCK_METHOD0(sendWifiConfiguredAck, void());
  MOCK_METHOD0(sendUrlServerConfiguredAck, void());
};