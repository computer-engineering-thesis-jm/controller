#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "mock_bluetooth_service.cpp"
#include "../source_header.h"
#include <string>

using ::testing::AtLeast;
using ::testing::Return;

TEST(ConfigureDeviceTest, saveDeviceInformationFromBluetoothWhenItsAvailable)
{
    // Given
    MockBluetoothService *mockBluetoothService = new MockBluetoothService();
    Configuration *configuration = new Configuration();

    ConfigureDevice *configureDevice = new ConfigureDevice(mockBluetoothService, configuration);

    const char* deviceId = "aDeviceId";
    const char* tokenId = "aTokenId";
    const char* ssid = "anSSID";
    const char* password = "aPassword";
    const char* endpoint = "anEndpoint";
    const char* port = "aPort";

    Device *device = new Device(deviceId, tokenId);
    WifiNetwork *wifiNetwork = new WifiNetwork(ssid, password);
    UrlServer *urlServer = new UrlServer(endpoint, port);

    EXPECT_CALL(*mockBluetoothService, connect())
        .Times(1);
    EXPECT_CALL(*mockBluetoothService, disconnect())
        .Times(1);
    EXPECT_CALL(*mockBluetoothService, isAvailable())
        .Times(3)
        .WillRepeatedly(Return(true));

    EXPECT_CALL(*mockBluetoothService, readDeviceConfiguration())
        .Times(1)
        .WillOnce(Return(device));

    EXPECT_CALL(*mockBluetoothService, readWifiConfiguration())
        .Times(1)
        .WillOnce(Return(wifiNetwork));
    
    EXPECT_CALL(*mockBluetoothService, readUrlServerConfiguration())
        .Times(1)
        .WillOnce(Return(urlServer));

    EXPECT_CALL(*mockBluetoothService, sendDeviceConfiguredAck()).Times(1);
    EXPECT_CALL(*mockBluetoothService, sendWifiConfiguredAck()).Times(1);
    EXPECT_CALL(*mockBluetoothService, sendUrlServerConfiguredAck()).Times(1);
    EXPECT_CALL(*mockBluetoothService, sendCompletedAck()).Times(1);

    // When
    configureDevice->execute();

    // Then
    Device *actualDevice = configuration->getDevice();
    WifiNetwork *actualWifiNetwork = configuration->getWifiNetwork();
    UrlServer *actualUrlServer = configuration->getUrlServer();
    ASSERT_STREQ(device->getId(), actualDevice->getId());
    ASSERT_STREQ(device->getToken(), actualDevice->getToken());
    ASSERT_STREQ(wifiNetwork->getSsid(), actualWifiNetwork->getSsid());
    ASSERT_STREQ(wifiNetwork->getPassword(), actualWifiNetwork->getPassword());
    ASSERT_STREQ(urlServer->getEndpoint(), actualUrlServer->getEndpoint());
    ASSERT_STREQ(urlServer->getPort(), actualUrlServer->getPort());

    delete mockBluetoothService;
    delete configuration;
    delete configureDevice;
    delete device;
    delete wifiNetwork;
    delete urlServer;
    delete actualDevice;
    delete actualWifiNetwork;
    delete actualUrlServer;
}