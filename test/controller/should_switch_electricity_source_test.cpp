#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>

#include "../source_header.h"

#include "mock_sensor.cpp"
#include "mock_electricity_source.cpp"

using ::testing::AtLeast;
using ::testing::Return;

TEST(ShouldSwitchElectricitySourceTest, registerAndUpdateElectricitySourceStatusFromSensor)
{
    // Given
    MockSensor *mockSensor = new MockSensor();
    MockElectricitySource *mockElectricitySource = new MockElectricitySource();
    Metrics *metrics = new Metrics();

    ShouldSwitchElectricitySource *shouldSwitchElectricitySource = new ShouldSwitchElectricitySource(mockSensor, mockElectricitySource, metrics);

    int sensorValue = 10;
    EXPECT_CALL(*mockSensor, get())
        .Times(1)
        .WillOnce(Return(sensorValue));

    EXPECT_CALL(*mockElectricitySource, update(sensorValue))
        .Times(1);
    
    // When
    shouldSwitchElectricitySource->execute();

    // Then
    const char *expectedName = "electricity_source";
    const char *expectedValue = std::to_string(sensorValue).c_str();
    ElectricitySourceMetric *actualElectricitySourceMetric = metrics->getElectricitySourceMetric();
    ASSERT_STREQ(expectedName, actualElectricitySourceMetric->getName());
    ASSERT_STREQ(expectedValue, actualElectricitySourceMetric->getValue());

    delete mockSensor;
    delete mockElectricitySource;
    delete shouldSwitchElectricitySource;
    delete actualElectricitySourceMetric;
}