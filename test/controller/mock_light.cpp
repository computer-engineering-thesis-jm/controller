#include "gmock/gmock.h"
#include "../source_header.h"

class MockLight : public Light
{
public:
  MOCK_METHOD0(initialize, void());
  MOCK_METHOD1(update, void(int));
};