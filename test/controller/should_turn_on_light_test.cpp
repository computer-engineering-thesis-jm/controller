#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>

#include "../source_header.h"

#include "mock_sensor.cpp"
#include "mock_light.cpp"

using ::testing::AtLeast;
using ::testing::Return;

TEST(ShouldTurnOnLightTest, registerAndUpdateLightStatusFromSensor)
{
    // Given
    MockSensor *mockSensor = new MockSensor();
    MockLight *mockLight = new MockLight();
    Metrics *metrics = new Metrics();

    ShouldTurnOnLight *shouldTurnOnLight = new ShouldTurnOnLight(mockSensor, mockLight, metrics);

    int sensorValue = 1;
    EXPECT_CALL(*mockSensor, get())
        .Times(1)
        .WillOnce(Return(sensorValue));

    const char *valueAsChar = std::to_string(sensorValue).c_str();

    EXPECT_CALL(*mockLight, update(sensorValue))
        .Times(1);
    
    // When
    shouldTurnOnLight->execute();

    // Then
    const char *expectedName = "light";
    const char *expectedValue = std::to_string(sensorValue).c_str();
    LightMetric *actualLightMetric = metrics->getLightMetric();
    ASSERT_STREQ(expectedName, actualLightMetric->getName());
    ASSERT_STREQ(expectedValue, actualLightMetric->getValue());

    delete mockSensor;
    delete mockLight;
    delete shouldTurnOnLight;
    delete actualLightMetric;
}