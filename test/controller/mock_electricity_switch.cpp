#include "gmock/gmock.h"
#include "../source_header.h"

class MockElectricitySource : public ElectricitySwitch
{
public:
  MOCK_METHOD1(update, void(int));
};