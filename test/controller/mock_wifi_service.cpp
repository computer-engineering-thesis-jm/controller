#include "gmock/gmock.h"
#include "../source_header.h"

class MockWifiService : public WifiService
{
public:
  MOCK_METHOD0(initialize, void());
  MOCK_METHOD0(initialized, bool());
  MOCK_METHOD2(connect, int(const char *, const char *));
  MOCK_METHOD0(connectedStatus, int());
  MOCK_METHOD0(disconnectedStatus, int());
  MOCK_METHOD0(getConnectionStatus, const char*());
};