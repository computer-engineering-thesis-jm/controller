// Actions
#include "../src/controller/core/action/configure_device.hpp"
#include "../src/controller/core/action/register_metrics.hpp"
#include "../src/controller/core/action/connect_wifi.hpp"
#include "../src/controller/core/action/start_sensors.hpp"
#include "../src/controller/core/action/should_turn_on_light.hpp"
#include "../src/controller/core/action/should_switch_electricity_source.hpp"

// Model
#include "../src/controller/core/model/device.hpp"
#include "../src/controller/core/model/wifi_network.hpp"
#include "../src/controller/core/model/url_server.hpp"

// Repositories
#include "../src/controller/core/repository/configuration.hpp"
#include "../src/controller/core/repository/metrics.hpp"

// Services
#include "../src/controller/core/service/bluetooth_service.hpp"
#include "../src/controller/core/service/wifi_service.hpp"
#include "../src/controller/core/service/metric_service.hpp"

// Components
#include "../src/controller/component/extra/light.hpp"
#include "../src/controller/component/extra/electricity_switch.hpp"
#include "../src/controller/component/sensor/sensor.hpp"
#include "../src/controller/component/sensor/metric/basic_metric.hpp"
#include "../src/controller/component/sensor/metric/light_metric.hpp"
#include "../src/controller/component/sensor/metric/electricity_switch_metric.hpp"

