# Renewable energy controller

Arduino Project to storage, control and remote monitoring of renewable solar energy.

It was used Platform IO Framework to build and upload binaries to Arduino Board.

## Platform IO

#### Installation:

```console
> pip install -U platformio
```

#### Configuration:

```
[env:megaatmega2560]
platform = atmelavr
board = megaatmega2560
framework = arduino
```

#### Install libraries

```console
> platformio lib install ###
```

#### Libraries required

```
ACS712 Current Sensor
=====================
#ID: 1601

Adafruit Circuit Playground
===========================
#ID: 602

Adafruit TSL2561
================
#ID: 34

Adafruit Unified Sensor
=======================
#ID: 31

ArduinoJson
===========
#ID: 64

LiquidCrystal
=============
#ID: 887

SoftwareSerial
==============
#ID: 2728

WiFi
====
#ID: 870

WiFiEsp
=======
#ID: 509
```
## Arduino 

#### Peripherals
- Bluetooth - HC-05
- Wifi - ESP8266
- Protoboard with Power Supply for Wifi Module - Mb102

#### Pins
- Serial1 - Wifi
- Serial2 - Bluetooth

### References:
- [Platform IO - Installation](https://docs.platformio.org/en/latest/installation.html)
- [Platform IO - Arduino Mega Configuration](https://docs.platformio.org/en/latest/boards/atmelavr/megaatmega2560.html#hardware)
- [Platform IO - Library installation](https://docs.platformio.org/en/latest/userguide/lib/cmd_install.html)